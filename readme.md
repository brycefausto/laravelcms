# LaravelCMS

The app is a simple CMS template built from scratch using Laravel and Vue.The CMS will be able to make new posts, update existing posts, delete posts that we do not need anymore, and also allow users make comments to posts which will be updated in realtime using Pusher. We will also be able to add featured images to posts to give them some visual appeal.