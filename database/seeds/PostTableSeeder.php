<?php

use Illuminate\Database\Seeder;
use App\Post;

class PostTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Post::create([
            'user_id' => 1,
            'title' => 'Using Laravel Seeders',
            'body' => "Laravel includes a simple method of seeding your database with test data using seed classes. 
            All seed classes are stored in the database/seeds directory. 
            Seed classes may have any name you wish, but probably should follow some sensible convention, 
            such as UsersTableSeeder, etc. By default, a DatabaseSeeder class is defined for you. 
            From this class, you may use the  call method to run other seed classes, allowing you to control the 
            seeding order."
        ]);

        sleep(1);

        Post::create([
            'user_id' => 1,
            'title' => 'Database: Migrations',
            'body' => "Migrations are like version control for your database, allowing your team to easily modify and share the 
            application's database schema. Migrations are typically paired with Laravel's schema builder to easily build 
            your application's database schema. If you have ever had to tell a teammate to manually add a column to their 
            local database schema, you've faced the problem that database migrations solve."
        ]);

        sleep(1);

        Post::create([
            'user_id' => 1,
            'title' => 'Some Post',
            'body' => "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et 
            dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo 
            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. 
            Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
            'image' => 'some-post.jpg'
        ]);
    }
}
