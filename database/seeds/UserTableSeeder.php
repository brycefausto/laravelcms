<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Role;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = User::create([
            'name' => 'Admin User',
            'email' => 'admin@mail.com',
            'password' => bcrypt('admin123'),
        ]);
        $admin->roles()->attach(Role::where('name', 'admin')->first());

        $user = User::create([
            'name' => 'Bryce Fausto',
            'email' => 'bryce@mail.com',
            'password' => bcrypt('pass123'),
        ]);
        $user->roles()->attach(Role::where('name', 'user')->first());
    }
}
